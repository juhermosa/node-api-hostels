"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const room_model_1 = require("../models/room.model");
const db = firebase_admin_1.default.firestore();
const hotelsRef = db.collection('hotels');
const roomsRef = db.collection('rooms');
async function createRoom(hotelId, newRoom) {
    checkArgs(hotelId, newRoom);
    const hotelRef = hotelsRef.doc(hotelId);
    const hotelSnap = await hotelRef.get();
    if (!hotelSnap.exists) {
        throw new Error('hotel does not exists');
    }
    const createdRoomRef = await roomsRef.add(newRoom);
    const createdRoomInHotelRef = hotelRef
        .collection('rooms')
        .doc(createdRoomRef.id);
    await createdRoomInHotelRef.set({ ref: createdRoomRef, uid: createdRoomRef.id });
    return 'ok';
}
exports.createRoom = createRoom;
async function deleteRoom(hotelId, roomIdToDelete) {
    checkArgs(hotelId, roomIdToDelete);
    const hotelRef = hotelsRef.doc(hotelId);
    const hotelSnap = await hotelRef.get();
    if (!hotelSnap.exists) {
        throw new Error('hotel does not exists');
    }
    const roomToDeleteRef = roomsRef.doc(roomIdToDelete);
    const roomSnap = await roomToDeleteRef.get();
    if (!roomSnap.exists) {
        throw new Error('room does not exists');
    }
    await roomToDeleteRef.delete();
    const createdRoomInHotelRef = hotelRef
        .collection('rooms')
        .doc(roomIdToDelete);
    await createdRoomInHotelRef.delete();
    return 'ok';
}
exports.deleteRoom = deleteRoom;
async function getRoomById(roomId) {
    if (!roomId) {
        throw new Error('roomId is missing');
    }
    const roomRef = roomsRef.doc(roomId);
    const roomSnap = await roomRef.get();
    if (!roomSnap.exists) {
        throw new Error(`room ${roomId} does not exists`);
    }
    return roomSnap.data();
}
exports.getRoomById = getRoomById;
function checkRoomState(state) {
    return Object.keys(room_model_1.RoomState).some((roomState) => roomState === state);
}
async function setRoomByState(roomId, state) {
    if (!roomId) {
        throw new Error('roomId is missing');
    }
    if (!checkRoomState(state)) {
        throw new Error('state is invalid');
    }
    const roomRef = roomsRef.doc(roomId);
    const roomSnap = await roomRef.get();
    if (!roomSnap.exists) {
        throw new Error(`room ${roomId} does not exists`);
    }
    await roomRef.update({ roomState: state });
    return 'ok';
}
exports.setRoomByState = setRoomByState;
async function getRoomState(roomId) {
    const room = await getRoomById(roomId);
    return room.roomState;
}
exports.getRoomState = getRoomState;
async function getAllRoomsByUids(roomsUids) {
    const promisesToResolve = roomsUids
        .map(uid => getRoomById(uid));
    return await Promise.all(promisesToResolve);
}
exports.getAllRoomsByUids = getAllRoomsByUids;
function checkArgs(arg1, arg2) {
    if (!arg1 || !arg2) {
        throw new Error('args missing');
    }
}
//# sourceMappingURL=rooms.service.js.map