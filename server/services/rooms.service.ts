import admin from 'firebase-admin';
import {RoomModel, RoomState} from '../models/room.model';
import CollectionReference = FirebaseFirestore.CollectionReference;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import DocumentReference = FirebaseFirestore.DocumentReference;


const db = admin.firestore();
const hotelsRef: CollectionReference = db.collection('hotels');
const roomsRef: CollectionReference = db.collection('rooms');

export async function createRoom(hotelId: string, newRoom: RoomModel): Promise<string> {
    checkArgs(hotelId, newRoom);
    const hotelRef: DocumentReference = hotelsRef.doc(hotelId);
    const hotelSnap: DocumentSnapshot = await hotelRef.get();
    if (!hotelSnap.exists) {
        throw new Error('hotel does not exists');
    }
    const createdRoomRef: DocumentReference = await roomsRef.add(newRoom);
    const createdRoomInHotelRef: DocumentReference = hotelRef
        .collection('rooms')
        .doc(createdRoomRef.id);
    await createdRoomInHotelRef.set({ref: createdRoomRef, uid: createdRoomRef.id});

    return 'ok';
}


export async function deleteRoom(hotelId: string, roomIdToDelete: string): Promise<string> {
    checkArgs(hotelId, roomIdToDelete);
    const hotelRef: DocumentReference = hotelsRef.doc(hotelId);
    const hotelSnap: DocumentSnapshot = await hotelRef.get();
    if (!hotelSnap.exists) {
        throw new Error('hotel does not exists');
    }
    const roomToDeleteRef: DocumentReference = roomsRef.doc(roomIdToDelete);
    const roomSnap: DocumentSnapshot = await roomToDeleteRef.get();
    if (!roomSnap.exists) {
        throw new Error('room does not exists');
    }
    await roomToDeleteRef.delete();
    const createdRoomInHotelRef: DocumentReference = hotelRef
        .collection('rooms')
        .doc(roomIdToDelete);
    await createdRoomInHotelRef.delete();

    return 'ok';
}


export async function getRoomById(roomId: string): Promise<RoomModel> {
    if (!roomId) {
        throw new Error('roomId is missing');
    }
    const roomRef: DocumentReference = roomsRef.doc(roomId);
    const roomSnap: DocumentSnapshot = await roomRef.get();
    if (!roomSnap.exists) {
        throw new Error(`room ${roomId} does not exists`);
    }
    return roomSnap.data() as RoomModel;
}

function checkRoomState(state: any) {
    return Object.keys(RoomState).some((roomState) => roomState === state);
}

export async function setRoomByState(roomId: string, state: RoomState): Promise<string> {
    if (!roomId) {
        throw new Error('roomId is missing');
    }
    if (!checkRoomState(state)) {
        throw new Error('state is invalid');
    }
    const roomRef: DocumentReference = roomsRef.doc(roomId);
    const roomSnap: DocumentSnapshot = await roomRef.get();
    if (!roomSnap.exists) {
        throw new Error(`room ${roomId} does not exists`);
    }
    await roomRef.update({roomState: state});
    return 'ok';
}

export async function getRoomState(roomId: string): Promise<RoomState | undefined> {
    const room: RoomModel = await getRoomById(roomId);
    return room.roomState;
}


export async function getAllRoomsByUids(roomsUids: string[]) {
    const promisesToResolve: Promise<RoomModel>[] = roomsUids
        .map(uid => getRoomById(uid));
    return await Promise.all(promisesToResolve);
}

function checkArgs(arg1: any, arg2: any) {
    if (!arg1 || !arg2) {
        throw new Error('args missing');
    }
}


